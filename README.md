<h2>Overview</h2>

XFlix is a video sharing platform which hosts videos for the world to watch. It also features uploading new videos by using external video links (eg: Youtube) and playing these videos.

During the course of this project,
    Built XFlix backend using Node.js, Express.js and MongoDB from scratch
    Implemented APIs according to the API contract set

<h3> Scope of work  </h3>
<p>Implemented a set of 5 REST APIs - </p>

    
    GET /v1/videos
    GET /v1/videos/:videoId
    POST /v1/videos
    PATCH /v1/videos/:videoId/votes
    PATCH /v1/videos/:videoId/views
    

Improved `GET /v1/videos` endpoint to allow
- video title search
- filtering by multiple genres
- filtering by content rating
- sorting by video upload date or view count

Utilized MongoDB to persist video data <br/>

Plugged-in the backend to XFlix frontend and deployed the Full-stack application

<b>Skills used - </b> <br /> 
`Node.js` `Express.js` `MongoDB` `REST API` `Postman` 

Request variants supported by <b>“GET /v1/videos”</b> endpoint

